package com.demo.oauth2.security.web.controller;

import com.demo.oauth2.security.web.config.CustomTokenGranter;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeAccessTokenProvider;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.util.OAuth2Utils;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.*;

import static org.springframework.security.oauth2.common.exceptions.OAuth2Exception.UNAUTHORIZED_CLIENT;

@RestController
public class DemoController {

    @Autowired
    private TokenEndpoint tokenEndpoint;

    @Autowired
    private TokenGranter tokenGranter;

    @Autowired
    private ClientDetailsService clientDetailsService;

    @Autowired
    private AuthorizationCodeServices authorizationCodeServices;



    @RequestMapping("/test")
    public String test(){
        return "test";
    }


    // 处理授权请求  （  不用开放URL 登录访问  ）
    @GetMapping("/hello/authorize")
    public String authorize(Principal principal,
                            @RequestParam(value = "client_id")  String clientId,
                            @RequestParam(value = "redirect_uri", required = false) String redirectUri,
                            @RequestParam(value = "state", required = false) String state,
                            @RequestParam(value = "response_type", defaultValue = "code") String responseType,
                            HttpServletRequest request,
                            HttpServletResponse response) throws IOException {

        String authorizationCode = null;

        // 创建授权请求
        ClientDetails clientDetails = clientDetailsService.loadClientByClientId(clientId);
        Set<String> scopes = clientDetails.getScope();
        Set<String> responseTypes = new HashSet<>();
        responseTypes.add(responseType);
        Map<String, String> authorizationParameters = new HashMap<>();
        authorizationParameters.put("client_id", clientId);
        authorizationParameters.put("response_type", responseType);
        authorizationParameters.put("redirect_uri", redirectUri);
        AuthorizationRequest authorizationRequest = new AuthorizationRequest(authorizationParameters, Collections.emptyMap(), clientId, scopes, (Set)null, (Collection)null, false, state, redirectUri, responseTypes);
        authorizationRequest.setResourceIdsAndAuthoritiesFromClientDetails(clientDetails);
        authorizationRequest.setRedirectUri(redirectUri);
        // 将授权请求设置为当前安全上下文中的OAuth2Authentication
        Authentication authentication = (Authentication)principal;
        // 创建授权码
        try {
            OAuth2Request storedOAuth2Request = authorizationRequest.createOAuth2Request();
            OAuth2Authentication combinedAuth = new OAuth2Authentication(storedOAuth2Request, authentication);
            authorizationCode = authorizationCodeServices.createAuthorizationCode(combinedAuth);
        } catch (Exception e) {
            e.printStackTrace();
        }


        String redirectUrl = redirectUri + "?code=" + authorizationCode;
        if (state != null) {
            redirectUrl += "&state=" + state;
        }
        // 重定向到重定向URL
        response.sendRedirect(redirectUrl);
        return null;
    }


    /* 请求类型可以自定改动  建立改为POST  */  //（匿名访问）
    @SneakyThrows
    @PostMapping(value = "/yeah/getToken")
    public OAuth2AccessToken getToken(@RequestParam(value = "client_id") String client_id,
                                       @RequestParam(value = "redirect_uri") String redirect_uri,
                                       @RequestParam(value = "code") String code,
                                       @RequestParam(value = "grant_type") String grant_type) {

        //accessTokenRequest.set("state", "123"); // 替换为实际的随机 state 值

        //OAuth2AccessToken accessToken = restTemplate.getAccessToken();
        Map<String, String> parameters = new HashMap<>();
        parameters.put("client_id", client_id);
        parameters.put("redirect_uri", redirect_uri);
        parameters.put("code", code);
        parameters.put("grant_type", grant_type);
        ClientDetails clientDetails = clientDetailsService.loadClientByClientId(client_id);
        Set<String> scope = clientDetails.getScope();
        TokenRequest tokenRequest = new TokenRequest(parameters, client_id, scope, grant_type);
        OAuth2AccessToken token = tokenGranter.grant(grant_type, tokenRequest);
        return token;
    }



}

package com.demo.oauth2.security.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.InMemoryAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
@EnableAuthorizationServer
public class OAuth2Config extends AuthorizationServerConfigurerAdapter {


//    @Autowired
//    @Qualifier("demoUserDetailsService")
//    public UserDetailsService demoUserDetailsService;

//    @Autowired
//    private DataSource dataSource;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Autowired
    private TokenStore jwtTokenStore;

    @Autowired
    private JwtAccessTokenConverter jwtAccessTokenConverter;

    @Autowired
    private TokenEnhancer jwtTokenEnhancer;

    @Bean
    public CustomTokenGranter customCompositeTokenGranter(List<TokenGranter> tokenGranters) {
        return new CustomTokenGranter(tokenGranters);
    }

    /*关键在于授权码 需要同一个实例的存储和获取 */
    @Bean
    public AuthorizationCodeServices authorizationCodeServices(){
        //内存
        return new InMemoryAuthorizationCodeServices();
        //JdbcAuthorizationCodeServices
    }

    @Override
    public void configure(final AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        /**
         * jwt 增强模式
         */
        TokenEnhancerChain enhancerChain = new TokenEnhancerChain();
        List<TokenEnhancer> enhancerList = new ArrayList<>();
        enhancerList.add(jwtTokenEnhancer);
        enhancerList.add(jwtAccessTokenConverter);
        enhancerChain.setTokenEnhancers(enhancerList);
        endpoints.tokenStore(jwtTokenStore);
        // 获取原有默认授权模式(授权码模式、密码模式、客户端模式、简化模式)的授权者
        List<TokenGranter> granterList = new ArrayList<>(Arrays.asList(endpoints.getTokenGranter()));

                //.userDetailsService(demoUserDetailsService)
                /**
                 * 支持 password 模式
                 */

        endpoints.authenticationManager(authenticationManager)
                .tokenEnhancer(enhancerChain)
                .accessTokenConverter(jwtAccessTokenConverter).tokenGranter(customCompositeTokenGranter(granterList)).authorizationCodeServices(authorizationCodeServices());

    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient("xql")
                //客户端 秘钥 以及加密方式BCryptPasswordEncoder
                .secret(new BCryptPasswordEncoder().encode("xql123"))
                //客户端拥有的资源列表
                .resourceIds("res1")
                //该client允许的授权类型
                .authorizedGrantTypes("authorization_code",
                        "password", "client_credentials", "implicit",
                        "refresh_token")
                //允许的授权范围
                .scopes("all")
                //跳转到授权页面
                .autoApprove(false)
                //回调地址
                .redirectUris("http://localhost:8081/oauth2-server/login");
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) {
//        security.allowFormAuthenticationForClients();
//        security.checkTokenAccess("isAuthenticated()");
//        security.tokenKeyAccess("isAuthenticated()");

//        security.tokenKeyAccess("permitAll()")
//                .checkTokenAccess("permitAll()")
//                .passwordEncoder(passwordEncoder);

        security.tokenKeyAccess("permitAll()")
                .checkTokenAccess("isAuthenticated()")
                .allowFormAuthenticationForClients();

//        security.addTokenEndpointAuthenticationFilter(
//                new BasicAuthenticationFilter(authenticationManager, customAuthenticationEntryPoint));
    }
}
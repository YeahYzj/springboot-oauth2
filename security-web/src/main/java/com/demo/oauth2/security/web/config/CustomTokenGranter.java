package com.demo.oauth2.security.web.config;

import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.stereotype.Service;

import java.util.List;


public class CustomTokenGranter extends CompositeTokenGranter {


    public CustomTokenGranter(List<TokenGranter> tokenGranters) {
        super(tokenGranters);
    }

    @Override
    public OAuth2AccessToken grant(String grantType, TokenRequest tokenRequest) {
        return super.grant(grantType, tokenRequest);
    }
}

package com.demo.oauth2.security.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.InMemoryAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.RandomValueAuthorizationCodeServices;

@SpringBootApplication
public class SpringBootSecurityWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSecurityWebApplication.class, args);
    }



    @Bean
    public RandomValueAuthorizationCodeServices RandomValueAuthorizationCodeServices() {
        return new InMemoryAuthorizationCodeServices();
    }


}
